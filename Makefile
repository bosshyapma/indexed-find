CXX          = g++
CXX_FLAGS    = -Wall -Werror -Wextra
LD           = ld
LN           = ln
RM           = rm
CHMOD        = chmod
GZIP         = gzip
STRIP        = strip
CP           = cp
PROJECT_NAME = indexed-find
IS_INSTL_MAN = false

.PHONY       = target.out clean all install clean-compiled clean-check check distclean strip manpage doc

all: target.out
	$(CXX) main.cpp target.out $(CXX_FLAGS) -o $(CXX).$(PROJECT_NAME) # compile
	$(LN) -sfv $(CXX).$(PROJECT_NAME) $(PROJECT_NAME) 

strip: ; $(STRIP) -sv $(PROJECT_NAME)

target.out: clean-target.out
	echo CXX_FLAGS:  $(CXX_FLAGS) > target.o 2>&1
	echo $(CXX) -v:  >> target.o 2>&1
	$(CXX) -v >> target.o 2>&1
	$(LD) -r -b binary -o target.out target.o

manpage: ; $(GZIP) -k -v -c ./$(PROJECT_NAME).1 > ./$(PROJECT_NAME).1.gz
doc: manpage

clean: ; $(RM) -rvf *$(PROJECT_NAME) *.out *.o *.gz test-folder/ test-folder-old/

clean-target.out : ; $(RM) -rvf target.out target.o

clean-compiled: ; $(RM) -rvf *$(PROJECT_NAME) *.out *.o *.gz

clean-check: ; $(RM) -rvf test-folder/ test-folder-old/

check:
	$(CHMOD) +x ./test-suite.sh && ./test-suite.sh

install: # had a thought on creating an entire bash script just for this section because how make's if-else works
	@echo "--------------------"
ifeq ($(shell PATH="$(PATH):/data/data/com.termux/files/usr/bin" which termux-info), /data/data/com.termux/files/usr/bin/termux-info)
	@echo "Termux installation detected (or \"which termux-info\" lied)"
	@echo "Will install to \"/data/data/com.termux/files/usr/bin/$(PROJECT_NAME)\" no matter what"
	$(CP) -v $(PROJECT_NAME) /data/data/com.termux/files/usr/bin/$(PROJECT_NAME)
      ifeq ($(IS_INSTL_MAN), true)
	@echo "Installing man page too..."
	$(CP) -v ./$(PROJECT_NAME).1.gz /data/data/com.termux/files/usr/share/man/man1/$(PROJECT_NAME).1.gz
      endif
	@echo "--------------------"
else ifeq ($(shell whoami), root)
	@echo "Root detected, installing to \"/usr/bin/$(PROJECT_NAME)\""
	$(CP) -v $(PROJECT_NAME) /usr/bin/$(PROJECT_NAME)
      ifeq ($(IS_INSTL_MAN), true)
	@echo "Installing man page too..."
	$(CP) -v ./$(PROJECT_NAME).1.gz /usr/share/man/man1/$(PROJECT_NAME).1.gz
      endif
	@echo "--------------------"
else
	@echo "Non-root detected, installing to \"~/.local/bin/$(PROJECT_NAME)\""
	$(CP) -v $(PROJECT_NAME) ~/.local/bin/$(PROJECT_NAME)
      ifeq ($(IS_INSTL_MAN), true)
	@echo "Installing man page too..."
	$(CP) -v ./$(PROJECT_NAME).1.gz ~/.local/share/man/man1/$(PROJECT_NAME).1.gz
      endif
	@echo "--------------------"
endif
