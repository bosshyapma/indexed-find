# Indexed Find, Search fast with indexing.

[![Build Status](https://travis-ci.com/bosshyapma/indexed-find.svg?branch=master)](https://travis-ci.com/bosshyapma/indexed-find)
[![If you are seeing this, then shields.io didn't load, report it and go to bintray manually](https://img.shields.io/static/v1.svg?label=Download%20the%20latest%20binary%20from&message=Bintray&color=brightgreen)](https://dl.bintray.com/bosshyapma/indexed-find)  

## There is a lot of bad code, I know it and I will try to fix them.

> fun fact: most of this program (about %95) is written from phone.  
> also no longer testing about manpage in travis, sorry

Indexed Find is a program that indexes using **du**, and searches through already-made indexes using **grep** shows them using **less or stdout**.
This version is 1.0.

### Requirements:

##### Required:
1. **du** or **find** for listing and indexing.
2. **grep** for sorting from index.
3. **less** for listing results.

##### Optional:
4. **cron** for running automatically.

### How does it work?

##### Short answer:

1. When indexing, call **du** or **find** to list-index.  
2. When searching, call **grep** to sort from index and use **less** or **output to terminal (stdout)** to list.  
3. If wanted, write results to a file too.

##### Long answer:

1. When indexing, it calls program **du** (from **GNU coreutils**) to list everything in a directory and save the listings into a file which is the index file.  
An index file just contains full path names to files, you can make one manually too by writing the full path names pointing to some files like this into a file (change *mypersonallenovo* with your username):  
> /home/mypersonallenovo/Documents/todo-list.txt  
> /home/mypersonallenovo/Documents/list-of-useful-programs.txt  
> /home/mypersonallenovo/Documents/rss-feeds.txt  
If you decide to use **find** (from **GNU findutils**), then **find** will not add sizes of folder/files to the start of them (**find** only prints out names), meaning that you cannot look to sizes of the files you are searching for, but it will take shorter to index since **find** doesn't calculate the sizes of all the files inside *to-be-index*'ed folder.  
2. When searching from an index file, it first calls **grep** (from **GNU grep**) to sort which lines (or file-directory names) contains the given search term and output them.  
Then, by default it calls **less** to list the output that **grep** has given.  
3. You can also decide to **output to terminal (stdout)** and **write results to a file** through arguments.

### Compiling (for Linux):

1. `make`  
2. `./indexed-find`

For running tests, `make check`.  
For man page, `make doc` or `make manpage`.  
For installing, `make install` or `sudo make install`.  
If you are on Termux, then `make install` will send to */data/data/com.termux/files/usr/bin*,  
or if you are non-sudo, *~/.local/bin*, or if you are sudo, */usr/local/bin*.  
If you want to install manpage within `make install`, tell make as `make install IS_INSTALL_MAN=true`.  
### Environment variables:

##### Program variables (small list):

 1. IFIND\_DU : Overrides where `du` will be found and used.  
 2. IFIND\_GREP : Same above except for `grep`.   
 3. IFIND\_LESS : Same above for `less`.  
 4. IFIND\_FIND : Same but for `find`.

##### Makefile variables (medium list):

 1. CXX : C++ Compiler.  
 2. CXX\_FLAGS : C++ Flags.  
 3. LD : Linker (for converting compiled-for info to linkable format).  
 4. LN : **symboilc-**linker for creating `./indexed-find` to the compiled program.  
 5. RM : `make clean`.  
 6. CHMOD : Sets `test-suite.sh` to e(**-x**)ecutable.  
 7. GZIP : `gzip` for manpage.  
 8. STRIP : Tool `strip` (in case you want to save some space).  
 9. CP : Copier (for installing).  
 10. IS\_INSTALL\_MAN : Is `make install` going to install manpage, too? (`IS_INSTL_MAN=true` for **yes**, any other for **no**).

### Some notes:

1. This program only automates the commands to be called to automate the process of index-searching.  
2. Since indexes needs to be made automatically, use `cron` when using **indexed-find**.  
3. If running under a multi user machine (for example, if the machine is being used by other users) and you want to index things below your home directory, **DO NOT RUN AS ROOT!!!** It will leak other users personal files which is a bad thing to do. Run as root **if you really need to (most of the time you don't need to) or if you are the only user on the machine.**  
4. Indexing the entire root file system *(I was the only user, look the text above)* took about a minutes and the index file was about 90Mb, so if you are low on space, then think twice when using:  
> username@hostname:~/indexed-find$ time indexed-find -o ~/.indexed/main-index -u /     
> Now indexing directory: / to /home/username/.indexed/main-index  
> Please wait...  
>
> /home/username/.indexed/main-index  
>
> Successfully reindexed "/"!  
>
> real  0m57,948s  
> user  0m1,912s  
> sys   0m3,813s  
> username@hostname:~/indexed-find$ ls ~/.indexed/main-index  
> 88M -rw-rw-r-- 1 username username 88M Eki 17 20:41 /home/username/.indexed/main-index  
> username@hostname:~/indexed-find$  

### License

Indexed Find is licensed under [GNU General Public License v3.0 (and later)](https://www.gnu.org/licenses/gpl-3.0.en.html), click or see the `LICENSE` file for details.
