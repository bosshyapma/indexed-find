#define RESET         "\033[0m"
#define RED           "\033[31m"       // error
#define GREEN         "\033[32m"       // success
#define YELLOW        "\033[33m"       // warning
#define BLUE          "\033[34m"       // info
#define MAGENTA       "\033[35m"       // command outputs
#define IFIND_VERSION "1.0" // Version string
#include <iostream>                    //std::cout
#include <stdlib.h>                    //system
#include <string>                      //std::string
#include <sstream>                     //std::stringstream
#include <fstream>                     //open
using namespace std;                   // shorten std:: calls

extern char _binary_target_o_start; //compiled with stuff
extern char _binary_target_o_end; //...end

int du_block_size=1; //if --du-block-size is not used
bool indexx=false, helpp=false, version=false, searcch=false, use_less=true,
     sdout=false, file=false, silent=false, verbose=false, use_temp=false,
     use_du=true, is_grep=false, is_find=false, is_less=false, is_du=false; // set at argcheck(), used at argcommit()
string buffr, launchedas, searchterm, filename, less_env,
       find_env, grep_env, du_env, tmp_dir,
       iindex = "~/.indexed/main_index", oindex = "~/.indexed/main_index", placetoindex; // storing env. variables, argument values
stringstream bufr; // for launch() and l_output()
int launch(){ //will get bufr sstream and verbose bool from global commands
  int result; //create local variable for holding system() result
  result=system(bufr.str().c_str()); //launch given sstream and save result to variable
  if (verbose) cout << BLUE << "Exit status for \"" << MAGENTA << bufr.str() << BLUE << "\" is: " << result << RESET << endl; // if verbose, then print exit code
  bufr.str(""); bufr.clear(); //clear sstream
  return result; //speaks for itself
}

void log(string to_log) {
  if (verbose) cout << BLUE << "indexed-find-debug: " << to_log << RESET << endl;
}

string l_output(){
  string result;
  if (!use_temp) {
    char buffer[128];
    FILE* pipe = popen(bufr.str().c_str(), "r");
    if (!pipe) {
      cerr << RED << "Opening a pipe from \"" << bufr.str() << "\" output to \"" << launchedas << "\" (or this program)! Exiting..." << endl <<
      "If you don't want to use pipes, then specify a temp.dir by \"--tmp <temp folder>\"." << RESET << endl;
      exit(1);
    }
    else {
      bufr.str(""); bufr.clear(); // reset buf(fe)r since this function wont do it
      while (!feof(pipe)) {
        if (fgets(buffer, 128, pipe) != NULL) result += buffer;
      }
    }
    pclose(pipe);
  }
  else if (use_temp) {
    bufr << " > " << tmp_dir << "/ifind_output 2>&1"; launch();
    ifstream output((tmp_dir+"/ifind_output").c_str());
    if (output.good())
    {
      getline(output, result);
    }
    output.close();
    bufr << "rm " << tmp_dir << "/ifind_output";
    if (verbose) bufr << " -v";
    else if (silent) bufr << " > /dev/null 2>&1";
    launch();
  }
  return result;
}

void check_apps(){
  //check if find, grep, less and du exists (check for env. variables too)
  if (!use_du) {
    log("Now checking if \"find\" is available and in the correct version...");
    bufr << "which ";
    if (getenv("IFIND_FIND") != NULL) {
      find_env = getenv("IFIND_FIND");
      if (find_env == "") find_env = "find";
      if (verbose) cout << BLUE << "IFIND_FIND is: \"" << find_env << "\"" << RESET << endl;
    }
    else find_env = "find";
    bufr << find_env;
    if (!verbose) bufr << " > /dev/null 2>&1";
    if (launch() != 0) {
      cerr << RED << "\"--use-find\" is specified, but \"find\" couldn't be found! Leaving..." << RESET << endl;
    }
    else {
      bufr << find_env << " --version";
      if (l_output().substr(0, 21) != "find (GNU findutils) "){
        cerr << RED << "\"" << find_env << "\" failed \"--version\" test! Please install one from \"findutils\" by GNU. Leaving..." << RESET << endl;
        exit(1);
      }
      else is_find = true;
    }
  }

  log("Now checking if \"du\" is available and in the correct version...");
  bufr << "which ";
  if (getenv("IFIND_DU") != NULL) {
    du_env = getenv("IFIND_DU");
    if (du_env == "") du_env = "du";
    if (verbose) cout << BLUE << "IFIND_DU is: \"" << du_env + "\"" << endl;
  }
  else du_env = "du";
  bufr << du_env;

  if (!verbose) bufr << " > /dev/null 2>&1";
  if (launch() != 0) is_du = false;
  else {
    bufr << du_env << " --version";
    if (l_output().substr(0, 19) != "du (GNU coreutils) "){
      cerr << RED << "\"" << du_env << "\" failed \"--version\" test! Please install one from \"coreutils\" by GNU. Leaving..." << endl << RESET;
      exit(1);
    }
    else is_du = true;
  }

  log("Now checking if \"grep\" is available and in the correct version...");
  bufr << "which ";
  if (getenv("IFIND_GREP") != NULL) {
    grep_env = getenv("IFIND_GREP");
    if (grep_env == "") grep_env = "grep";
    if (verbose) cout << BLUE << "IFIND_GREP is: \"" << grep_env << "\"" << RESET << endl;
  }
  else grep_env = "grep";
  bufr << grep_env;
  if (!verbose) bufr << " > /dev/null 2>&1";
  if (launch() != 0) is_grep = false;
  else {
    bufr << grep_env << " --version";
    if (l_output().substr(0, 16) != "grep (GNU grep) "){
      cerr << RED << "\"" << grep_env << "\" failed \"--version\" test! Please install one from \"grep\" by GNU. Leaving..." << RESET << endl;
      exit(1);
    }
    else is_grep = true;
  }

  log("Now checking if \"less\" is available and in the correct version...");
  bufr << "which ";
  if (getenv("IFIND_LESS") != NULL) {
    less_env = getenv("IFIND_LESS");
    if (less_env == "") less_env = "less";
    if (verbose) cout << BLUE << "IFIND_LESS is: \"" << less_env << "\"" << endl << RESET;
  }
  else less_env = "less";
  bufr << less_env;
  if (!verbose) bufr << " > /dev/null 2>&1";

  if (launch() != 0) is_less = false;
  else {
    bufr << less_env << " --version";
    buffr = l_output();
    if (buffr.substr(0, 5) != "less " && buffr.substr(9, 35) != " (GNU regular expressions) "){
      cerr << RED <<"\"" << less_env << "\" failed \"--version\" test! Please install one from \"http://www.greenwoodsoftware.com/less/\". Leaving..." << RESET <<  endl;
      exit(1);
    }
    else is_less = true;
  }
  if (find_env != "" && verbose) cout << BLUE << "(IFIND_FIND variable is: \"" << find_env << "\")" << RESET << endl;
  if (du_env != "" && verbose) cout << BLUE << "(IFIND_DU variable is: \"" << du_env << "\")" << RESET << endl;
  if (grep_env != "" && verbose) cout << BLUE << "(IFIND_GREP variable is: \"" << grep_env << "\")" << RESET << endl;
  if (less_env != "" && verbose) cout << BLUE << "(IFIND_LESS variable is: \"" << less_env << "\")" << RESET << endl;
  if (!is_less || !is_grep || !is_du) {
    cerr << RED << "This program needs " << endl;
      if (!is_find) {
        cerr << "\"find\" (from package \"findutils\") for indexing."<< endl;
        if (getenv("IFIND_FIND") != NULL) cerr << "(IFIND_FIND variable is: \"" << find_env << "\")" << endl;
      }
      if (!is_du) {
        cerr << "\"du\" (from package \"coreutils\") for indexing (if \"find\" isn't available)."<< endl;
        if (getenv("IFIND_DU") != NULL) cerr << "(IFIND_DU variable is: \"" << du_env << "\")" << endl;
      }
      if (!is_grep) {
        cerr << "\"grep\" (from package \"grep\") for finding given search term in index file." << endl;
        if (getenv("IFIND_GREP") != NULL) cerr << "(IFIND_GREP variable is: \"" << grep_env << "\")" << endl;
      }
      if (!is_less) {
        cerr << "\"less\" (from package \"less\") for viewing search results! Please install them first." << endl;
        if (getenv("IFIND_LESS") != NULL) cerr << "(IFIND_LESS variable is: \"" << less_env << "\")" << endl;
      }
    cerr << RESET;
    exit(1);
  }

}
void reindex(){
  // First check if the folder is reachable and a real folder.
  log(("Now starting to log place \"" + placetoindex + "\" to the file \"" + oindex + "\"...").c_str());
  bufr << "ls "<<placetoindex<<"/";
    if (verbose==true) bufr << " -la"; //verbose enabled, add listings
    else if (verbose==false) bufr << " >/dev/null 2>&1"; //disabled, suppress

  if (launch() != 0) {
    // if not, then throw error and exit(1);
    cerr << RED << "Given place-to-index \"" << placetoindex << "\" isn't valid or we don't " << endl <<
      "have permission to read it (not running as root-sudo?)!" << endl
      << "Please give a valid location such as \"/\" or retry with root(sudo)." << RESET <<endl;
    exit(1);
  }
  else {
    // if its a folder, then,
    if (!silent) cout << BLUE << "Now indexing directory: "<< placetoindex <<" to "<<
      oindex<< endl <<"Please wait..."<<RESET<<endl; // 0. Tell user "I'm indexing here!".

    bufr << "ls " << oindex; // 1. See if old index file exits
    if (verbose==true) bufr << " -la";
      if (launch() == 0) {
        bufr << "rm " << oindex; // 2. Remove old index file.
        if (verbose==true) bufr << " -v"; // (if verbose, enable "rm -v" flag, rm is default silent)
        if(launch() != 0){
          cerr << RED << "Removing given old index file failed (we couldn't just append to it), non-permitted(non-sudo)?" << RESET <<endl;
          exit(1);
        }
      }

    bufr << "mkdir -p " << oindex; // ... creates folder with index file name (to be deleted)....
      if (verbose==true) bufr << " -v "; // (if verbose, enable "mkdir -v" flag, mkdir is default silent)
    bufr << "&& rm -rf " << oindex; // then delete that folder so we can create file
      if (verbose==true) bufr << " -v "; // (if verbose, enable "rm -v" flag, rm is default silent)
    if (launch() != 0){
      cerr << RED << "Creating folder for index file failed. non-sudo?" << RESET <<endl;
      exit(1);
    }

    bufr << "bash -c \"";
    if (use_du) bufr << du_env << " -a --time --block-size "<< du_block_size <<" --apparent-size ";
    else if (!use_du) bufr << find_env << " -P ";
    bufr <<placetoindex<<" > " << oindex << " 2>&1 \""; // ... and list everything inside given folder with rule "don't follow symlinks"
    launch();
      // Finally run the command.
    if (!silent) cout << GREEN << endl << endl <<
      "Successfully reindexed \"" << placetoindex <<
      "\" to \"" << oindex << "\"!" << RESET << endl;
      // And output "Everything's good."
  }
}
void showversion(){
  cout << "indexed-find v" << IFIND_VERSION << " (launched as: \""<< launchedas <<"\"), licensed under GNU GPLv3." << endl << // tell version and license
    "See \"https://gitlab.com/bosshyapma/indexed-find\" for source code," << endl << // github repo
    "\"https://www.gnu.org/licenses/gpl-3.0.en.html\" for the license used and" << endl << // license file on gnu.org
    "\"https://travis-ci.com/gitlab/bosshyapma/indexed-find\" for if latest commit builded correctly." << endl << endl << // travisci results
    "This program (" << launchedas <<") is compiled using the following information:" << endl;
  char* address_data = &_binary_target_o_start;
  while ( address_data != &_binary_target_o_end ) cout << *address_data++;
  exit(0);
}
void showhelp(){
  cout << "indexed-find (launched with command: " << launchedas << ")" << endl << // show help+launched as
    "Usage: " << launchedas << " [options] (-u <where to index> / -s <search term>)" << endl << endl <<
    "Main arguments                      :" << endl <<
    "  -u / --index <wheretoindex>       : Indexes a location (default: /, override using <wheretoindex>) to ~/.indexed/main_index (override with -o)" << endl <<
    "  -s / --search <searchterm>        : Searches <searchterm> using grep in ~/.indexed/main_index (override with -i)" << endl <<
    "  -h / --help                       : Show this help message." << endl <<
    "  -V / --version                    : Show version and license." << endl << endl <<
    "Arguments for indexing (-u)         :" << endl <<
    "  -o / --output-index <filename>    : Overrides where an index will be saved to with <filename>. (use with -u)" << endl <<
    "       --use-find                   : Use tool \"find\" instead of \"du\" when indexing." << endl <<
    "       --du-block-size <block size> : When \"--use-du\", specify how much a block will be counted." << endl <<
    "         real size / <blocksize> = outputted size, 1 for bytes, 1024 for KBs, 1024000 for MBs... , default is 1." << endl << endl <<
    "Arguments for searching (-s)        :" << endl <<
    "  -l / --less                       : Use \"less\" (default) when showing search results. (use with -s)" << endl <<
    "  -d / --stdout                     : Output search results to terminal instead of \"less\". (use with -s)"<< endl <<
    "  -f / --file <filename>            : Write search results (again, with -s) to <filename> file." << endl <<
    "  -i / --input-index <filename>     : Overrides where search with -s will be performed." << endl << endl <<
    "Arguments for both                  :" << endl <<
    "       --tmp <temp. folder>         : Use \"read-write to a file in <temp. folder>\" when requiring output instead of in app pipes (not recommended)" << endl <<
    "  -S / --silent                     : Enable silent mode." << endl <<
    "  -v / --verbose                    : Enable verbose mode (don't silence command outputs)." << endl <<
    "You can also use the following environment variables: " << endl <<
    "  IFIND_DU : Override where \"du\" will be found." << endl <<
    "  IFIND_GREP : Override where \"grep\" will be found." << endl <<
    "  IFIND_LESS : Override where \"less\" will be found." << endl <<
    "  IFIND_FIND   : Override where \"find\" will be found." << endl;
  exit(0);
}
void argcheck(int argc, char *argv[]){
  for (int i=1; i < argc; ++i){
    buffr = argv[i];
    if (buffr=="-u" || buffr=="--index"){ //if argument is index
      indexx=true;
      i++;
      if (argv[i]==NULL) {
        placetoindex="~/"; //if next one is null then use home dir
        cout << YELLOW << "Not given place-to-index, selecting home dir \"~/\"." << RESET << endl; //and report
      }
      else if (argv[i][0]=='-'){ //if not null but starts with a dash (-) then exit and say its an argument
        cerr << RED << "Given place-to-index argument is another argument, leaving..." << RESET << endl;
        exit(1);
      }
      else placetoindex=argv[i]; //if not null and not argument then save it for later checking
    }
    else if (buffr=="-s" || buffr=="--search"){ //if argument is search
      if (argv[i+1]==NULL) { //if null then exit
        cerr << RED << "No search term given! Try \"indexed-find -s my_files\"." << RESET << endl;
        exit(1);
      }
      else if (argv[i+1][0] == '-'){ //if argument then exit
        cerr << RED << "Search term is an argument, cancelling..." << endl <<
        "Tip: If the file (or directory) that you search is only one character (.../-/...)," << endl <<
        "then try with \"-s /-\" which will not start with dash(-), therefore making it not an argument." << RESET << endl;
        exit(1);
      }
      else { //
        searchterm=argv[i+1];
        searcch=true;
        i++;
      }
    }
    else if (buffr=="-l" || buffr=="--less"){
      sdout=false;
      use_less=true;
    }
    else if (buffr=="-d" || buffr=="--stdout"){
      sdout=true;
      use_less=false;
    }
    else if (buffr=="-f" || buffr=="--file"){
      file=true;
      filename=argv[i+1];
      i++;
    }
    else if (buffr=="-o" || buffr=="--output-index"){
      oindex=argv[i+1];
      i++;
    }
    else if (buffr=="-i" || buffr=="--input-index"){
      iindex=argv[i+1];
      i++;
    }
    else if (buffr=="-v" || buffr=="--verbose") {
        verbose = true;
        silent =false;
    }
    else if (buffr=="-S" || buffr=="--silent") {
      silent=true; verbose=false;
    }
    else if (buffr=="-h" || buffr=="--help") showhelp();
    else if (buffr=="-V" || buffr=="--version") showversion();
    else if (buffr=="--tmp") {
      if (argv[i+1] == NULL) {
        if (!silent) cout << YELLOW << "Given temp. folder argument is empty, using \"/tmp/\"..." << RESET << endl;
        buffr = "/tmp/";
      }
      else {i++; buffr = argv[i];}

      if (buffr[0] == '-') {
        cerr << RED << "Given temp. folder starts with \"-\" (argument start), exiting..." << RESET << endl;
        exit(1);
      }
      else if (buffr == "/") {
        cerr << RED <<"Given temp. folder is root directory! (what?)" << endl <<
        "Exiting... (don't expect coder to write to root folder.)" << RESET << endl;
        exit(1);
      }
      else {
        if (buffr == "NULL") {
          if (!silent) cout << YELLOW << "Given temp. folder name is \"NULL\", assuming \"/tmp/\"" << endl <<
          "If you are going to use a real folder named \"NULL\", then specify it as \"... --tmp ./NULL/\" ..." << RESET << endl;
          buffr = "/tmp/";
      	}
        bufr << "echo -n \"qwertyasdfgh\" > " << buffr << "/ifind-test 2>&1";
        if (launch() != 0) {
          cerr << RED << "Writing to temp. file (to see if temp. folder works) failed! Please specify another one. Exiting..." << RESET << endl;
          exit(1);
        }
        else {
          bufr << "cat " << buffr << "/ifind-test"; launch();
          // copied straight from c57b109 commit's main.cpp
          ifstream output((buffr+"/ifind-test").c_str());
          string sLine;
          if (output.good())
          {
            getline(output, sLine);
          }
          output.close();
          bufr << "rm " << buffr << "/ifind-test"; launch();
          // copying ends
          if (sLine != "qwertyasdfgh") {
            cerr << RED << "\rTesting temp. folder failed! Exiting..." << RESET << endl;
            exit(1);
          }
          else {
            tmp_dir = buffr;
            use_temp = true;
            cout << "\r";
            if (buffr != "/tmp/") cout << YELLOW << "Using temp. folder \"" << tmp_dir << "\"..." << RESET << endl;
          }
        }
      }
    }
    else if (buffr=="--use-find") {
      use_du=false;
    }
    else if (buffr=="--du-block-size") {
      if (!use_du) {
        cerr << RED << "\"--use-find\" is specified, exiting..." << RESET << endl;
        exit(1);
      }
      else if (argv[i+1]==NULL) {
        cout << YELLOW << "No block size given, defaulting to 1..." << RESET << endl;
      }
      else if (argv[i+1][0] == '-'){ //if argument then exit
        cerr << RED << "Given block size is an argument (bytes can't be negative, right?), exiting..." << RESET << endl;
        exit(1);
      }
      else {
        i++; buffr = argv[i];
        stringstream geek(buffr);
        geek >> du_block_size;
        if (du_block_size == 0){
          cerr << RED << "You know that it's impossible to:" << endl <<
          "1. divide by zero (real size / 0 (given-processed block size)) or" << endl <<
          "2. give a character/string as a number?" << endl <<
          "Because the processed number resulted to 0 for some reason." << endl <<
          "I'm exiting..." << RESET << endl;
          exit(1);
        }
      }
    }
    else {
      cerr << RED << "Couldn't find argument: \"" << buffr << "\"." << endl <<
      "You might want to see the help message by : \""<< launchedas << " -h\" or \"" << launchedas <<" --help\"" << RESET << endl;
      exit(1);
    }
  }
}
void argcommit(){
  if (searcch == true) {
    bufr << grep_env << " \"" << searchterm << "\" "<< iindex;
    if (use_less == true) {
      if (!silent) cout << BLUE << "Will use \"less\" for search results (-l).\n" << RESET << endl;
      bufr << " | " << less_env;
      launch();
    }
    else if (sdout == true) {
      if (!silent) cout << BLUE << "Will use output to terminal (stdout) (-d) rather than using \"less\".\n" << RESET << endl;
      launch();
    }
    if (file == true) {
      if (!silent) cout << BLUE << "Will also print to file (-f): " << filename << RESET << endl;
      bufr << grep_env << " \"" << searchterm << "\" "<< iindex << " > " << filename << " 2>&1";
      launch();
    }
  }
  else if (indexx == true) {
    reindex();
  }
}
int main(int argc, char *argv[]){
  launchedas = argv[0]; //send argv[0] to string for globalness
  if (argv[1] == NULL) {
    cerr << RED << "No argument given, leaving..." << endl << "Please look at help text: \"" << launchedas<< " --help\"." << endl << RESET;
    exit(1);
  }
  else argcheck(argc, argv);
  check_apps();
  if (!searcch && !indexx) {
    cerr << RED << "Wasn't told whether to index or to search, leaving..." << RESET << endl;
    exit(1);
  }
  if (searcch && indexx){
    cerr << RED << "Didn't implement index-and-search yet. Sorry :(" << RESET << endl;
    exit(1);
  }
  else argcommit();
}

