#!/usr/bin/env bash
if [ "$(cat test-folder-old/logs/statuss)" = "test in process" ]; then
  echo "Old test folder detected! Probably the previous test failed."
  echo "Remove it first using \"make clean-check\"."
  exit 1
elif [ "$(cat test-folder/logs/statuss)" = "test in process" ]; then
  echo "A test in progress! Exiting..."
  exit 1
elif [ "$($(which ls) indexed-find)" != "indexed-find" ]; then
  echo "Binary not compiled! Compile it using: \"make\"."
  exit 1
else
  export IFIND_VERSION_CHECK=$(indexed-find -V)
  export IFIND_VERSION_CHECK=${$IFIND_VERSION_CHECK:0:32}
  if [ "$IFIND_VERSION_CHECK" != "indexed-find v1.0 (launched as: " ]; then
    echo "This test suite is designed for indexed-find v1.0!"
    echo "Please use that version, exiting..."
    exit 1
  fi
fi
echo "(1/2) Preparing environment..."

mkdir test-folder/env  \
      test-folder/logs -p
pushd test-folder/
pushd env/

# create a lot of folders
mkdir aa/bb/cc/dd/ee/ff/ \
      aa/cc/bb/ff/dd/ee/ \
      ff/aa/ee/dd/bb/cc/ \
      bb/cc/dd/ff/cc/aa/ \
      bb/cc/bb/bb/cc/ff/ \
      ff/bb/ff/ee/ff/bb/ \
      ee/ee/aa/ff/bb/cc/ \
      cc/ff/ff/aa/aa/aa/ \
      bb/aa/ee/cc/cc/dd/ -p

# copy some empty files info these folders
echo "" > bb/fefwe
echo "" > aa/bb/f44
echo "" > ff/aa/ee/wfhu
echo "" > cc/gg/aa/aa/fwref
echo "" > ee/ee/aa/ff/bb/per
echo "" > ff/aa/ee/dd/bb/cc/dqdw

# and create dummy symlinks
ln -sf ff/aa/                 bb/cc/
ln -sf cc/gg/aa/aa/fwref      ff/bb/ffs
ln -sf aa/bb/f44              bb/aa/ee
ln -sf ff/aa/ee/dd/bb/cc/dqdw fwrer
ln -sf ee/ee/aa/ff/bb/per     dwsd
ln -sf ff/aa/ee/wfhu          3ggrr
ln -sf bb/fefwe               ewfw

# And create expected outputs
popd
pushd logs/
echo "test in process" > statuss 2>&1
find -P ../env/ > index
du -a --apparent-size --time --block-size 1 ../env/ > du_index1 2>&1
du -a --apparent-size --time --block-size 128 ../env/ > du_index2 2>&1
du -a --apparent-size --time --block-size 1024 ../env/ > du_index3 2>&1
grep "f" index > grep-f 2>&1

# start tests
echo ""
echo ""
echo "(2/2) Now testing compiled binary..."
echo ""
echo ""

# indexing
../../indexed-find -S --use-find -u ../env/ -o ifind-find
../../indexed-find -S --du-block-size 1 -u ../env/ -o ifind-du1
../../indexed-find -S --du-block-size 128 -u ../env/ -o ifind-du2
../../indexed-find -S --du-block-size 1024 -u ../env/ -o ifind-du3

# searching
../../indexed-find -S -d -f ifind_file_1 -i ifind-find -s "f" > ifind-file-1 2>&1

# using temp folder properly
../../indexed-find --tmp /tmp/ -V > /dev/null
export IFIND_TEST_4=$(echo $?)

if [ "$(cat ifind-find)" = "$(cat index)" ]; then
  echo "(1/4) Indexing (find) success PASS!"
  export IFIND_TEST_1=1
else
  echo "(1/4) Indexing (find) failed FAIL!"
  export IFIND_TEST_1=0
fi

if [ "$(cat ifind-du1)" = "$(cat du_index1)" ]; then
  if [ "$(cat ifind-du2)" = "$(cat du_index2)" ]; then
    if [ "$(cat ifind-du3)" = "$(cat du_index3)" ]; then
      echo "(2/4) Indexing (du) success PASS!"
      export IFIND_TEST_2=1
    fi
  fi
else
  echo "(2/4) Indexing (du) failed FAIL!"
  export IFIND_TEST_2=0
fi

# because of a bug, will use outputted file "-f" instead of the incoming output "-d".
if [ "$(cat ifind_file_1)" = "$(cat grep-f)" ]; then
  echo "(3/4) Searching success PASS!"
  export IFIND_TEST_3=1
else
  echo "(3/4) Searching failed FAIL!"
  export IFIND_TEST_3=0
fi
if [ $IFIND_TEST_4 = 0 ]; then
  echo "(4/4) Using temp folders PASS!"
  export IFIND_TEST_4=1
else
  echo "(4/4) Using temp folders FAIL!"
  export IFIND_TEST_4=0
fi
popd
popd
if [ "$(echo $IFIND_TEST_1)" = "0" ]; then
  echo "One of the tests didn't succeed. Renaming test-folder/ to test-folder-old/"
  mv test-folder/ test-folder-old/
  exit 1
elif [ "$(echo $IFIND_TEST_2)" = "0" ]; then
  echo "One of the tests didn't succeed. Renaming test-folder/ to test-folder-old/"
  mv test-folder/ test-folder-old/
  exit 1
elif [ "$(echo $IFIND_TEST_3)" = "0" ]; then
  echo "One of the tests didn't succeed. Renaming test-folder/ to test-folder-old/"
  mv test-folder/ test-folder-old/
  exit 1
else
  echo "All tests succeeded, removing test-folder/ ..."
  rm -rf test-folder/
  exit 0
fi
